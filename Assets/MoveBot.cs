﻿using UnityEngine;
using System.Collections;

public class MoveBot : MonoBehaviour {
	[Range(-1,1)]public float Speed;
	[Range(-1,1)]public float Direction;

	public float BaseSpeed;

	public Transform WheelRight;
	public Transform WheelLeft;

	public float lSpeed,rSpeed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {


		if (Direction >= 0.0f - Mathf.Epsilon && Direction <= 0.0f + Mathf.Epsilon) {
			//Debug.Log ("Going Forward");
			lSpeed = rSpeed = Speed;
		} else if (Direction > 0.0f + Mathf.Epsilon) {
			
			//Debug.Log ("Going Right");
			rSpeed = -(Speed * Direction);
			lSpeed = Speed;
		} else {
			//Debug.Log ("Going Left");
			lSpeed = (Speed * Direction);
			rSpeed = Speed;
		}

		Vector3 lForce = transform.forward * lSpeed * BaseSpeed;
		Vector3 rForce = transform.forward * rSpeed * BaseSpeed;

		GetComponent<Rigidbody> ().AddForceAtPosition (rForce , WheelRight.position);
		GetComponent<Rigidbody> ().AddForceAtPosition (lForce , WheelLeft.position);
	}
}
