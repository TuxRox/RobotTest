﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;

public class ConnectBot : MonoBehaviour {
	private SocketIOComponent socket;
	public string ChannelName;
	private MoveBot _MoveBot;

	public void Start() 
	{
		GameObject go = GameObject.Find("SocketIO");
		_MoveBot = GetComponent<MoveBot> ();

		socket = go.GetComponent<SocketIOComponent>();

		socket.On("open", HandleOpen);
		socket.On ("state", HandleState);
		socket.On("error", HandleError);
		socket.On("close", HandleClose);
	}
	public void HandleState(SocketIOEvent e){
		//Debug.Log ("[SocketIO] State recieved: " + e.name + " " + e.data);
		JSONObject obj = new JSONObject (e.data.ToString());
		string key = obj.GetField ("key").ToString().Trim('"');
		if (key.Equals ("speed")) {
			int speed;
			if (int.TryParse (obj.GetField ("value").ToString ().Trim ('"'), out speed)) {
				_MoveBot.Speed = (speed / 100.0f);
			}
		} else if (key.Equals ("direction")) {
			int direction;
			if (int.TryParse (obj.GetField ("value").ToString ().Trim ('"'), out direction)) {
				_MoveBot.Direction = (direction / 100.0f);
			}
		} else {
			Debug.Log ("Unrecognized key");
		}

	}
	public void HandleOpen(SocketIOEvent e)
	{
		Debug.Log("[SocketIO] Open received: " + e.name + " " + e.data);
		Dictionary<string,string> data = new Dictionary<string,string> ();
		data ["channel"] = ChannelName;

		socket.Emit ("join", new JSONObject (data));
	}

	public void HandleError(SocketIOEvent e)
	{
		Debug.Log("[SocketIO] Error received: " + e.name + " " + e.data);
	}

	public void HandleClose(SocketIOEvent e)
	{	
		Debug.Log("[SocketIO] Close received: " + e.name + " " + e.data);
	}


}
